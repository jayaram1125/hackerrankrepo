Problem Statement :

Given a time in AM/PM format, convert it to military (24-hour) time.

Note: Midnight is 12:00:00AM on a 12-hour clock and 00:00:00 on a 24-hour clock. Noon is 12:00:00PM on a 12-hour clock and 12:00:00 on a 24-hour clock.

Input Format

A time in 12-hour clock format (i.e.: hh:mm:ssAM or hh:mm:ssPM), where 01?hh?12.

Output Format

Convert and print the given time in 24-hour format, where 00?hh?23.

Sample Input

07:05:45PM
Sample Output

19:05:45
Explanation

7 PM is after noon, so you need to add 12 hours to it during conversion. 12 + 7 = 19. Minutes and seconds do not change in 12-24 hour time conversions, so the answer is 19:05:45.


Solution :

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */  
    string time ;
    cin >> time ;
    if("AM" == time.substr(8,2))
    {
      if("12" == time.substr(0,2)) 
      {
          string midtime ;
          midtime+= "00"+time.substr(2,6);
          cout<<midtime;
          midtime.clear();
      }
      else
      {
        cout<<time.substr(0,8);    
      }
    }
    else if("PM" == time.substr(8,2))
    {
      if("12" == time.substr(0,2)) 
      {
          cout<<time.substr(0,8);
      }
      else
      {
          string s = time.substr(0,2);
          int a = 0;
          a = (s[0] -'0')*10;
          a+= (s[1]-'0');
          a+=12;
          s.clear();
          stringstream ss ;
          ss << a;
          string s2; 
          s2 +=ss.str();
          s2 +=time.substr(2,6);
          cout<<s2;
          s2.clear();
      }
       
    }
    return 0;
}