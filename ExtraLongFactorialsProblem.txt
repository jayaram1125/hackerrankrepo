Problem Statement

You are given an integer N. Print the factorial of this number.

N!=N�(N?1)�(N?2)�?�3�2�1
Input 
Input consists of a single integer N, where 1?N?100.

Output 
Print the factorial of N.

Example 
For an input of 25, you would print 15511210043330985984000000.

Note: Factorials of N>20 can't be stored even in a 64?bit long long variable. Big integers must be used for such calculations. Languages like Java, Python, Ruby etc. can handle big integers, but we need to write additional code in C/C++ to handle huge values.

We recommend solving this challenge using BigIntegers.


#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;

Solution:

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */  
    int a[200];
for(int i = 0;i<200;++i)
{
	a[i] = 0;

}
a[199] =1 ;
int n;
cin >> n ;
for(int i =1 ; i<=n ;++i)
{

	for(int j = 199 ; j>0 ;--j)
	{
		a[j] = a[j]*i;
	}

	for(int j = 199 ; j>0 ;--j)
	{
		if(a[j] >=10)
		{

			a[j-1] += (a[j] /10) ;
			a[j] = a[j] %10 ;
		}
	}		
}

stringstream ss ;
for(int  i = 0 ;i<200 ;++i)
{

	ss << a[i] ;
}

string str  =  ss.str();
int pos = str.find_first_not_of('0');
string str2 = str.substr(pos,str.length()-pos);
cout<<str2;
    return 0;
}